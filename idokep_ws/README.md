# idokep.hu weather station data collector for local network

Collect idokep weather station data in local network. Sometimes there is a lack in network. It is possibly resulting data loss.
This script can collect the data on local network.

You need only cron, php and wget run on a server (e.g a Raspberry Pi).

The wind sensor collects data in every second, other sensors collect data in every minute. I suggest that you run the script in every minute. If you want to collect the wind sensor's data in every second you need another script.

It has not tested yet.
