<?php

# Based on weather_data_collector.php (openweathermap data collector script) by Attila Ferenc.

chdir(__DIR__);
$wsip = glob('wsip.txt');
if(empty($wsip)){
  exit("Error! I can't find the file contains the weather station ip address!" . PHP_EOL . "Add the wsip.txt file to this folder!" . PHP_EOL);
}

$csv = "ws_data.csv";
$timestamp = array (0=>"date;" . date("Y-m-d"), 1=>"time;" . date("H:i:s"));

#Downloading the data in local network with fix IP address. We presume, that there is only one weather station in the local network.

$wsip = file_get_contents('wsip.txt');
$wsip = str_replace(array("\r", "\n"), '', $wsip); /*We have to remove line breaks, if any.*/

$wind_data = exec ("wget http://$wsip/wind.json");
$sensors_data = exec ("wget http://$wsip/sensors.json");

/* For local testing only:
$wind_data = exec ("curl -O -C - $wsip/wind.json");
$sensors_data = exec ("curl -O -C - $wsip/sensors.json");
*/

$wind_data = file_get_contents('wind.json');
$sensors_data = file_get_contents('sensors.json');
system ("rm wind.json sensors.json"); /* We have to delete the files immediately.*/

# Formatting and merging the two json.

$wind_data = str_replace (array("\"", ",", " ", "{", "}"), '', $wind_data); /*Remove , from the end of lines, and " from values.*/
$sensors_data = str_replace (array("\"", ",", " ", "{", "}"), '', $sensors_data);
$wind_data = str_replace (array(":"), ';', $wind_data); /*We have to replace ':' to ';' (later the preg_replace in timestamp will be create bad results.)*/
$sensors_data = str_replace (array(":"), ';', $sensors_data);
$wind_data = explode("\n", $wind_data);
$sensors_data = explode("\n", $sensors_data);
# echo (PHP_EOL . "wind_data". PHP_EOL . PHP_EOL);
# var_dump($wind_data);
# echo (PHP_EOL . "sensors_data". PHP_EOL . PHP_EOL);
# var_dump($sensors_data);
$ws_data = array_merge ( $timestamp, $wind_data, $sensors_data);
$ws_data = array_filter ($ws_data, 'strlen'); #Remove empty elements.
# echo (PHP_EOL . "ws_data". PHP_EOL . PHP_EOL);
# var_dump ($ws_data);

# Create the column headers ant then the data rows in csv file.

$column_headers = $ws_data;
$column_headers = preg_replace('/(\S*)(;)(\S*)/', '$1', $column_headers);
# var_dump ($column_headers);

$ws_data = preg_replace('/(\S*)(;)(\S*)/', '$3', $ws_data);
# var_dump ($ws_data);

$csv_data = array_combine($column_headers, $ws_data);
# var_dump ($csv_data);

$header = array_keys($csv_data);
$rowdata = array_values($csv_data);

if (empty(file_exists($csv))){
  $fp = fopen($csv, 'w');
  fputcsv($fp, $header);
  fputcsv ($fp, $rowdata);
}
else {
  $fp = fopen($csv, 'a');
  fputcsv ($fp, $rowdata);
}
fclose($fp);

$datafiles = glob("ws_data.csv");
if(empty($datafiles)){
  exit("Warning! There is no created table." . PHP_EOL);
}

/*
# If you run cronjob another user then www-data, you may have to change created archives permissions to run occ command properly (in NextCloud server).
# You may need root privileges!
foreach ($datafiles as $datafile){
  system ("chgrp www-data $datafile");
  system ("chown www-data $datafile");
  system ("chmod 745 $datafile");
}
*/

# If you want to upload to a webdav public link, these rows could help you

$link_file = glob('upload_public_link.txt');
if(empty($link_file)){
  exit("Error! I can't find the file contains the webdav public link." . PHP_EOL . "Add the upload_public_link.txt file to this folder!" . PHP_EOL);
}
$datafiles = glob("ws_data.csv");
if(empty($datafiles)){
  exit("Warning! There is nothing to upload." . PHP_EOL);
}
$public_link = file_get_contents('upload_public_link.txt');
$public_link = explode(PHP_EOL, $public_link);
$key = preg_replace ('/(https:\/\/.+\/public.php\/webdav\/)(.+)/', '$2', $public_link[0]);
$url = preg_replace('/(https:\/\/.+\/public.php\/webdav\/)(.+)/', '$1', $public_link[0]);

#var_dump($key);
#var_dump($url);

foreach ($datafiles as $datafile) {
  exec ("curl -k -T $datafile -u \"$key:\" -H 'X-Requested-With: XMLHttpRequest' $url\/$datafile");
}

?>
