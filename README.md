# Weather Data Collector

Collect [OpenWeatherMap](https://openweathermap.org) data for specific location (e.g. each towns of a region).
The input is a csv table with the location names. I plan to use a POSTGIS layer with location names, or a postgis point layer with coordinates, etc.

Read locations from csv.
Results written to a csv file.

## Usage

### Main code

You have to create an api key and store it in the apikey.txt file.

You also have to create a locations.csv file, where you have to set the values like this:

```
location,country
Budapest,hu
Berlin,de
...
```

These files must be in the same folder, where the script is.

### Optional functions

There are two optional sections in the code. If you don't want to use these, you have to comment it out or you have to delete these lines!

First can modifiy user privileges (chgrp, chown, chmod). It could be useful, e.g. when you want to run the script as a cronjob.

Another is a webdav public link uploader.
If you want to use a webdav location (e.g. NextCloud or OwnCloud) to upload the result csv files, you have to create a folder with public link.
Your link will be something like this:

`https://nextcloud.someserver.org/public.php/webdav/SomeKey`

You have to store it in the upload_public_link.txt file, in the same folder, where the script is.

## More plans:

- Elegant field names for result csv file.
- Using more providers.
- Create other possibilities for inputs (e.g. POSTGIS).
- Write results to pgSQL/POSTGIS.
- Create a spatialite and/or POSTGIS layer for each locations.
- Support fieldwork (OBM mobilapp or QField)
  - In QGIS/QField it is possible auto fill the weather data field by location with a QField layer.
  - We must drop data older then 1 month or something depends on the size of the file (Only in field layer for QField).
