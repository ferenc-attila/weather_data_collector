<?php

#Inspired by Miklós Bán

# If there are any special national characters in the name of the location we have to exchange them.
function transliterateString($txt) {
    $transliterationTable = array('á'=>'a', 'Á'=>'A', 'é'=>'e', 'É'=>'E', 'í'=>'i', 'Í'=>'I','ó'=>'o', 'Ó'=>'O', 'ö'=>'o', 'Ö'=>'O', 'ő'=>'o', 'Ő'=>'O', 'ú'=>'u', 'Ú'=>'U', 'ű'=>'u', 'Ű'=>'U', 'ü'=>'u', 'Ü'=>'U');
    return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
}

# The json we get from openweathermap.org is multidimensional. We have to create a single array with unique fieldnames.
function RemoveDimensions($datagroups) {
  if (!isset($rd_datas)) {
    $rd_datas = array();
  }
  foreach ($datagroups as $key => $data) {
    if (is_array($data)){
      #var_dump($key);
      foreach ($data as $rd_data_key => $rd_data){
        # We have to create unique field names based on multidimensional array keys. We want keys like these: main.temp_min, wind.speed, etc.
        $rd_data_key = "$key.$rd_data_key";
        #var_dump($rd_data_key);
        $corrected_data = array($rd_data_key => $rd_data);
        #var_dump($corrected_data);
        $rd_datas = array_merge($rd_datas, $corrected_data);
      }
    #var_dump($rd_datas);
    }
    else {
      $rd_datas[$key] = $data;
    }
  }
  # var_dump($rd_datas);
  return ($rd_datas);
}

chdir(__DIR__);
$api_file = glob('apikey.txt');
$loc_file = glob('locations.csv');
if(empty($api_file)){
  exit("Error! I can't find the file contains the API key." . PHP_EOL . "Add the apikey.txt file to this folder!" . PHP_EOL);
}
elseif(empty($loc_file)){
  exit("Error! I can't find the file contains the locations." . PHP_EOL . "Add the locations.csv file to this folder!" . PHP_EOL);
}

$api_key = file_get_contents('apikey.txt');
$api_key = explode(PHP_EOL, $api_key);

$csv_input = file_get_contents('locations.csv');
$locations = explode (PHP_EOL, $csv_input);
foreach ($locations as $location) {
  if (empty($location)){
    $locations = array_diff($locations, array($location));
  }
}

array_shift($locations);

foreach ($locations as $location) {
  $locality = preg_replace('/(.+),(\w{2})/', '$1', $location);
  $lit_locality = transliterateString($locality);

  $csv = "weather_data.csv";
  #var_dump($location);
  $weather_data = exec ("curl -X GET \"https://api.openweathermap.org/data/2.5/weather?q=$location&APPID=$api_key[0]&units=metric&lang=hu\"");
  #var_dump($weather_data);

  $weather_data = json_decode($weather_data, TRUE);
  #var_dump($weather_data);

  $w_datas = RemoveDimensions($weather_data);
  $xy_data = RemoveDimensions($w_datas);

  # For human readeble timestamps:
  $timestamp_keys = array('dt', 'sys.sunrise', 'sys.sunset');
  foreach ($timestamp_keys as $timestamp_key) {
    $xy_data[$timestamp_key] = date('c', $xy_data[$timestamp_key]);
  }

  # Some values missing sometimes (e.g. if there is no rain, the rain.1h, rain.3h, etc. fields will be missing). We have to add n.a. value to this keys.
  $columns = array('coord.lon', 'coord.lat', 'weather.0.id', 'weather.0.main', 'weather.0.description', 'weather.0.icon', 'base', 'main.temp', 'main.feels_like', 'main.pressure', 'main.humidity', 'main.temp_min', 'main.temp_max', 'main.sea_level', 'main.grnd_level', 'visibility', 'wind.speed', 'wind.deg', 'cloud.all', 'rain.1h', 'rain.3h', 'snow.1h', 'snow.3h', 'dt', 'sys.type', 'sys.id', 'sys.message', 'sys.country', 'sys.sunrise', 'sys.sunset', 'timezone', 'id', 'name', 'cod');
  #var_dump($xy_data);
  $row = array();
  foreach ($columns as $column_header){
    if (array_key_exists($column_header, $xy_data)){
      $csv_data = array($column_header => $xy_data[$column_header]);
      #var_dump($csv_data);
      $row = array_merge ($row, $csv_data);
    }
    else {
      $csv_data = array($column_header => 'n.a.');
      $row = array_merge ($row, $csv_data);
    }
  }
  $addit_data = array('provider'=>'openweathermap');
  $row = array_merge($row, $addit_data);
  #var_dump($row);

  # Add values to a csv table (the scripts creates it, if neccessary):
  $header = array_keys($row);
  $rowdata = array_values($row);

  if (empty(file_exists($csv))){
    $fp = fopen($csv, 'w');
    fputcsv($fp, $header);
    fputcsv ($fp, $rowdata);
  }
  else {
    $fp = fopen($csv, 'a');
    fputcsv ($fp, $rowdata);
  }
  fclose($fp);
}

$datafiles = glob("weather_data.csv");
if(empty($datafiles)){
  exit("Warning! There is no created table." . PHP_EOL);
}

# If you run cronjob another user then www-data, you may have to change created archives permissions to run occ command properly.
# You may need root privileges!
foreach ($datafiles as $datafile){
  system ("chgrp www-data $datafile");
  system ("chown www-data $datafile");
  system ("chmod 745 $datafile");
}

# If you want to upload to a webdav public link, these rows could help you
$link_file = glob('upload_public_link.txt');
if(empty($link_file)){
  exit("Error! I can't find the file contains the webdav public link." . PHP_EOL . "Add the upload_public_link.txt file to this folder!" . PHP_EOL);
}
$datafiles = glob("weather_data.csv");
if(empty($datafiles)){
  exit("Warning! There is nothing to upload." . PHP_EOL);
}
$public_link = file_get_contents('upload_public_link.txt');
$public_link = explode(PHP_EOL, $public_link);
$key = preg_replace ('/(https:\/\/.+\/public.php\/webdav\/)(.+)/', '$2', $public_link[0]);
$url = preg_replace('/(https:\/\/.+\/public.php\/webdav\/)(.+)/', '$1', $public_link[0]);

#var_dump($key);
#var_dump($url);

foreach ($datafiles as $datafile) {
  exec ("curl -k -T $datafile -u \"$key:\" -H 'X-Requested-With: XMLHttpRequest' $url\/$datafile");
}

?>
